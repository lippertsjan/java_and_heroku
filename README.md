# Java 8 & Heroku

A custom docker image based on the [official OpenJDK image](https://hub.docker.com/_/openjdk/)
with added heroku CLI.

Source: https://gitlab.com/lippertsjan/java_and_heroku/