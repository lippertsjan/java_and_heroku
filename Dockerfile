# Java 8 & Heroku
# 
# A custom docker image based on the [official OpenJDK image](https://hub.docker.com/_/openjdk/)
# with added heroku CLI.
#
# 

FROM openjdk:9-jdk-slim-sid

RUN apt-get update && yes|apt-get install wget gnupg

RUN wget -qO- https://cli-assets.heroku.com/install-ubuntu.sh | sh
